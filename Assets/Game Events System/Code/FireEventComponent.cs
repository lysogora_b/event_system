﻿using UnityEngine;
using System.Collections;
using GameEventsSystem;

public class FireEventComponent : MonoBehaviour {
    public EventTag eventName;
    public virtual void FireEvent()
    {
        GES_Context.FireEvent(eventName, null);
    }
}
