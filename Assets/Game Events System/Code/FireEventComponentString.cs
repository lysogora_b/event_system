﻿using UnityEngine;
using System.Collections;
using GameEventsSystem;

public class FireEventComponentString : FireEventComponent {
    public string stringVal;
    public override void FireEvent()
    {
        GES_Context.FireEvent(eventName, new object[1]{stringVal});
    }
}
