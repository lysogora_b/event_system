﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEventSystemInterfaces
{
    public interface IBootstrap
    {
        void Initialize();
        void Deinitialize();
    }

    public interface GameEventsUser
    {
        void Initialize();
        void Deinitialize();
    }
}