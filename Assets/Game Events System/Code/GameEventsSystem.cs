﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace GameEventsSystem
{

	public class GES_Context
	{

	    private static GES_Context _instance;
	    public static GES_Context instance
	    {
	        get
	        {
	            if (_instance == null) _instance = new GES_Context();
	            return _instance;
	        }

	        set { _instance = value; }
	    }

	    public Dictionary<Type, IEventsSubManager> SubManagersByType = new Dictionary<Type, IEventsSubManager>();

        
	    #region static methods

        public static bool FireEvent(object eventName, params object[] data)
        {
            Type enumType = eventName.GetType();
              
            if (GES_Context.instance.SubManagersByType.ContainsKey(enumType))
            {
                GES_Context.instance.SubManagersByType[enumType].FireEvent(eventName, data);
            }
            else
            {
                Debug.Log("no subscribers for event of type "+enumType.ToString());
            }
            return false;
        }   

        public static bool SubscribeToEvent(object eventName, Action<object[]> callback)
        {
            Type enumType = eventName.GetType();

            if (!GES_Context.instance.SubManagersByType.ContainsKey (enumType)) 
            {
                GES_Context.RegisterEvent ((Enum)eventName);
            }
            GES_Context.instance.SubManagersByType[enumType].SubscribeToEvent(eventName, callback);
           
            return false;
        }

        public static bool UnSubscribeToEvent(object eventName, Action<object[]> callback)
        {
            Type enumType = eventName.GetType();

            if (GES_Context.instance.SubManagersByType.ContainsKey (enumType)) 
            {
                GES_Context.instance.SubManagersByType[enumType].UnSubscribeToEvent(eventName, callback);
            }
            return false;
        }

        public static bool RegisterEvent(Enum eventName)
        {
            Type enumType = eventName.GetType();

//            Debug.Log("Received enum type "+enumType.ToString());

            if (!GES_Context.instance.SubManagersByType.ContainsKey (enumType)) 
            {
                Type subManagerType = typeof(EventsSubManager<,>);

                Type finalType = subManagerType.MakeGenericType(new Type[2]{enumType, enumType});

                IEventsSubManager submanager = (IEventsSubManager)Activator.CreateInstance(finalType);

                GES_Context.instance.SubManagersByType.Add(enumType, submanager);
            }
            GES_Context.instance.SubManagersByType[enumType].RegisterEvent(eventName);

            return true;
        }

        public static bool SetGameFunction(object eventName, Func<object[], object[]> callback)
        {
            Enum e = (Enum)eventName;

            Type enumType = e.GetType();          

            if (!GES_Context.instance.SubManagersByType.ContainsKey (enumType)) 
            {
                Type subManagerType = typeof(EventsSubManager<,>);

                Type finalType = subManagerType.MakeGenericType(new Type[2]{enumType, enumType});

                IEventsSubManager submanager = (IEventsSubManager)Activator.CreateInstance(finalType);

                GES_Context.instance.SubManagersByType.Add(enumType, submanager);               
            }

            GES_Context.instance.SubManagersByType[enumType].SetGameFunction(eventName, callback);

            return true;
        }

        public static bool UnSetGameFunction(object eventName, Func<object[], object[]> callback)
        {
            Enum e = (Enum)eventName;

            Type enumType = e.GetType();          

            if (GES_Context.instance.SubManagersByType.ContainsKey (enumType)) 
            { 
                GES_Context.instance.SubManagersByType[enumType].UnsetGameFunction(eventName, callback);
            }
            return true;
        }

        public static object[] ExecuteGameFunction(object eventName, object[] data)
        {
            Enum e = (Enum)eventName;

            Type enumType = e.GetType();          

            if (GES_Context.instance.SubManagersByType.ContainsKey (enumType)) 
            { 
                if(GES_Context.instance.SubManagersByType[enumType] != null)
                {
                    return GES_Context.instance.SubManagersByType[enumType].ExecuteFunction(eventName, data);
                }
            }
            return null;
        }

        #endregion

        
	    #region Static generic Methods
        /// <summary>
        /// Calls the type of the generic mathod of. passs generic type as the first parameter of the data object array.
        /// </summary>
        /// <param name="instance">Instance.</param>
        /// <param name="instanceType">Instance type.</param>
        /// <param name="methodName">Method name.</param>
        /// <param name="data">Data.</param>
        public static void CallGenericMethodOfType(object instance, Type instanceType, string methodName, Type[] paramTypes, object[] passedArgs)
        {           
            if (passedArgs == null)
            {
                return;
            }

            if (paramTypes == null)
            {
                return;
            }

            System.Reflection.MethodInfo mInfo = instanceType.GetMethod(methodName).MakeGenericMethod(paramTypes);
            foreach (var item in mInfo.GetParameters())
            {
                Debug.Log(item.ParameterType.ToString());
            }
            foreach (var item in passedArgs)
            {
                Debug.Log(item.GetType());
            }
            mInfo.Invoke( instance, passedArgs );
        }

        public static object CallGenericFuncOfType(object instance, Type instanceType, string methodName, Type[] paramTypes, object[] passedArgs)
        {           
            if (passedArgs == null)
            {
                return null;
            }

            if (paramTypes == null)
            {
                return null;
            }

            return instanceType.GetMethod(methodName).MakeGenericMethod( paramTypes).Invoke( instance, passedArgs );
        }

        #endregion

        
	    #region Game functions

        #region static shortcut methods

        public static bool FireEvent(object eventName, object data)
        {
            GES_Context.FireEvent(eventName, new object[1]{data});

            return true;
        }

        public static bool FireEvent(object eventName, object arg1, object arg2)
        {
            GES_Context.FireEvent(eventName, new object[2]{arg1, arg2});

            return true;
        }

        public static R GetFunctionResult<R>(object functionName, object[] data)
        {
            Enum e = (Enum)functionName;

            Type enumType = e.GetType(); 

            object[] returnData = GES_Context.instance.SubManagersByType[enumType].ExecuteFunction(functionName, data);

            if (returnData == null || returnData.Length < 1)
            {
                return default(R);
            }
            R returnDatum = (R)returnData[0];

            return returnDatum;
        }

        public static R GetFunctionResult<R>(object functionName, object data)
        {
            Enum e = (Enum)functionName;

            Type enumType = e.GetType(); 

            object[] returnData = GES_Context.instance.SubManagersByType[enumType].ExecuteFunction(functionName, new object[1]{data});

            if (returnData == null || returnData.Length < 1)
            {
                return default(R);
            }
            R returnDatum = (R)returnData[0];

            return returnDatum;
        }

        public static R GetFunctionResult<R>(object functionName, object arg1, object arg2){
            Enum e = (Enum)functionName;

            Type enumType = e.GetType(); 

            object[] returnData = GES_Context.instance.SubManagersByType[enumType].ExecuteFunction(functionName, new object[2]{arg1, arg2});

            if (returnData == null || returnData.Length < 1)
            {
                return default(R);
            }
            R returnDatum = (R)returnData[0];

            return returnDatum;
        }

        #endregion

        #endregion

	}
    

    #region SubManagers

    public interface IEventsSubManager
    {
        bool FireEvent(object eventname, object[] data);

        bool RegisterEvent(object eventName);

        bool SubscribeToEvent(object eventName, Action<object[]> callback);

        bool UnSubscribeToEvent(object eventName, Action<object[]> callback);

        bool SetGameFunction(object functionName, Func<object[], object[]> callback);

        bool UnsetGameFunction(object functionName, Func<object[], object[]> callback);

        object[] ExecuteFunction(object functionName, object[] data);
    }

    public class EventsSubManager<eType, fType> : IEventsSubManager
    {
        #region Variables

        public Dictionary<eType, List<Action<object[]>>> registeredListenersByEvent = new Dictionary<eType, List<Action< object[]>>> ();

        public Dictionary<fType, Func<object[], object[]>> registeredFunctionsByEvent = new Dictionary<fType, Func<object[], object[]>> ();

        #endregion

        #region IEventsSubManager implementation

        public bool FireEvent(object eventname, object[] data)
        {
            eType eventEnum = (eType)eventname;

            if (eventEnum == null)
            {
                return false;
            }

            this.FireEvent(eventEnum, data);

            return true;
        }

        public bool RegisterEvent(object eventName)
        {
            eType eventEnum = (eType)eventName;

            if (eventEnum == null)
            {
                return false;
            }

            this.RegisterEvent(eventEnum);

            return true;
        }

        public bool SubscribeToEvent(object eventName, Action<object[]> callback)
        {
            eType eventEnum = (eType)eventName;

            if (eventEnum == null)
            {
                return false;
            }

            this.SubscribeToEvent(eventEnum, callback);

            return true;
        }

        public bool UnSubscribeToEvent(object eventName, Action<object[]> callback)
        {
            eType eventEnum = (eType)eventName;

            if (eventEnum == null)
            {
                return false;
            }

            this.UnSubscribeToEvent(eventEnum, callback);

            return true;
        }

        public bool SetGameFunction(object functionName, Func<object[], object[]> callback)
        {
            fType eventEnum = (fType)functionName;

            if (eventEnum == null)
            {
                return false;
            }

            this.SetGameFunction(eventEnum, callback);

            return true;
        }

        public bool UnsetGameFunction(object functionName, Func<object[], object[]> callback)
        {
            fType eventEnum = (fType)functionName;

            if (eventEnum == null)
            {
                return false;
            }

            this.UnsetGameFunction(eventEnum, callback);

            return true;
        }

        public object[] ExecuteFunction(object functionName, object[] data)
        {
            fType eventEnum = (fType)functionName;

            if (eventEnum == null)
            {
                return null;
            }

            return this.ExecuteFunction(eventEnum, data);
        }

        #endregion

        public bool FireEvent(eType eventname, object[] data)
        {
            if (registeredListenersByEvent.ContainsKey(eventname))
            {   
                for (int i = 0; i < registeredListenersByEvent[eventname].Count; i++) {
                    registeredListenersByEvent [eventname][i] (data);
                }
                return true;
            }
            return false;
        }       

        public bool RegisterEvent(eType eventName)
        {
            if (!registeredListenersByEvent.ContainsKey (eventName)) 
            {
                registeredListenersByEvent.Add(eventName, new List<Action<object[]>>());
            }
            return false;
        }

        public bool SubscribeToEvent(eType eventName, Action<object[]> callback)
        {
            if (!registeredListenersByEvent.ContainsKey (eventName)) 
            {
                RegisterEvent (eventName);
            }
            registeredListenersByEvent [eventName].Add(callback);
            return false;
        }

        public bool UnSubscribeToEvent(eType eventName, Action<object[]> callback)
        {
            if (registeredListenersByEvent.ContainsKey (eventName)) 
            {
                registeredListenersByEvent [eventName].Remove(callback);
            }
            return false;
        }


        #region Game functions

        public bool SetGameFunction(fType functionName, Func<object[], object[]> callback)
        {
//            Debug.LogError(this.GetType().ToString() + " SETTING FUNCTION "+functionName.ToString());
            if (!registeredFunctionsByEvent.ContainsKey (functionName)) 
            {
                registeredFunctionsByEvent.Add(functionName, null);
            }
            registeredFunctionsByEvent [functionName] = callback;
            return false;
        }

        public bool UnsetGameFunction(fType functionName, Func<object[], object[]> callback)
        {
            if (registeredFunctionsByEvent.ContainsKey (functionName)) 
            {
                registeredFunctionsByEvent [functionName] = null;
            }
            return false;
        }

        public object[] ExecuteFunction(fType functionName, object[] data)
        {
 //           Debug.LogError(this.GetType().ToString() + " EXECUTING FUNCTION "+functionName.ToString());
            if (registeredFunctionsByEvent.ContainsKey(functionName))
            {   
                if (registeredFunctionsByEvent[functionName] != null) 
                {
                    return registeredFunctionsByEvent [functionName] (data);
                }
            }
            return null;
        }

        #endregion

    }

    #endregion

    public enum GameDataStorage
    {
        SaveVisualScript,
        SaveBlueprintInstance,

        LoadVisualScript,
        LoadlueprintInstance,
        /// <summary>
        /// Arguments:  object data, string directoryPath, string filename
        /// </summary>
        SaveData,
        LoadData,
        /// <summary>
        /// The load data async. Parameters 0-Type, 1-data, 2-dir path, 3-file name, 4-callback
        /// </summary>
        SaveDataAsync,
        /// <summary>
        /// The load data async. Parameters 0-Type, 1-dir path, 2-file name, 3-callback
        /// </summary>
        LoadDataAsync,
    }

    public enum VisualScriptsDataStorage
    {
        SaveTextDataAsync,
        LoadTextDataAsync,

        LoadBlueprintInstance,
        SaveBlueprintInstance,

        LoadBlueprint,
        SaveBlueprint,

        GetObjectRelatedInstances,
        GetObjectRelatedBlueprints,

        GetObjectsLoadedInstances,
    }

    public enum FileManager_Event
    {
        ShowFilesList,

        GetFileNames,

        GetFileObjects, 

        GetFileStrings,

        GetFiles,

        RemoveFile
    }



}
