﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameEventsSystem;

public class GES_demo : MonoBehaviour
{
	// Use this for initialization
	void Start ()
	{
		// simple events
		// subscribe to event
		GES_Context.SubscribeToEvent(EventTag.Start, StartGame);
		// fire events
		GES_Context.FireEvent(EventTag.Start,23, 45);

		// functions (return value)
		// set function delegate
		GES_Context.SetGameFunction(DemoFunctionTag.GetSomeValue, GetDemoData);
		// request function result
		var someValue = GES_Context.GetFunctionResult<string>(DemoFunctionTag.GetSomeValue, null);
		Debug.Log("Demo data: "+someValue);

	}

	public void StartGame(params object[] data)
	{
		Debug.Log("Start demo "+(int) data[0]);
	}

	public object[] GetDemoData(params object[] data)
	{
		return new object[]{"This is event system demo"};
	}


}

public enum EventTag
{
	/// <summary>
	/// Start of the demo, Receive int argument
	/// </summary>
	Start,
}

public enum DemoFunctionTag
{
	/// <summary>
	/// get some string value
	/// </summary>
	GetSomeValue,
}
